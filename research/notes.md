# Sites
- https://ipip.ori.org/newBigFive5broadTable.htm
	+ Summary statistics of the IPIP Big 5 scale
- https://ipip.ori.org/InterpretingIndividualIPIPScaleScores.htm
	+ Interpreting Individual IPIP Scale Scores
- https://openpsychometrics.org/printable/big-five-personality-test.pdf
	+ IPIP Big 5 scoring guide
- https://www.r-bloggers.com/five-ways-to-calculate-internal-consistency/
	+ Five ways to calculate internal consistency