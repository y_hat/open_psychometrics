#!/bin/sh

# Script to download and unzip data files from openpsychometrics.org/_rawdata
#
# You can also download a specific dataset by including its name
# as an argument. The name should be as it is written in the "Downloads"
# column of the _rawdata page, WITHOUT the ".zip" extension.
#
# For example, to download the randomnumber dataset, run:
#
# ./download_data.sh randomnumber
#
# All data will be downloaded into the /data directory of the base
# project folder

outdir="$(dirname "$(pwd)")"/data
mkdir -p $outdir

if [ $# -eq 1 ]; then
	wget -r -np -A "${1}".zip -P $outdir https://openpsychometrics.org/_rawdata/

else
	wget -r -np -A.zip -P $outdir https://openpsychometrics.org/_rawdata/
	
fi

# Extract and remove zipped data
unzip $outdir/openpsychometrics.org/_rawdata/\*.zip -d $outdir
rm -r $outdir/openpsychometrics.org/
