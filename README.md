# Overview

This repo serves as a central hub for various scripts and other files used to
analyze data hosted at openpsychometrics.org. By design, every aspect of this
project should be completely reproducible--from downloading data to running
analyses, graphs, and documents--provided you are running a machine with
the necessary dependencies.

# Dependencies

- R 3.6.3 with the following packages installed:
    + tidyverse
    + here
    + psych
    + rvest (only needed for the metadata.R script)
- Various Unix utilites used for downloading, unzipping, and transforming 
data files, specifically:
    + sed
    + grep
    + cat
    + wget
- pdftotext

# Project Organization

The project is structured as follows:

```
proj/
├── analysis/ - Code for analyzing a particular dataset
├── data/ - Raw data downloaded from openpsychometrics.org/_rawdata
├── doc/ - LaTeX or other markup files for final reports/presentations
├── etc/ - Supplementary data files and documents for various datasets
├── figs/ - Figures and graphs
├── output/ - Output files, transformed data, summary statistics
├── scripts/ - Shell or R scripts used for ETLs and other purposes
├── research/ - Notes and relevant literature
└── .here - Used to mark project base directory. See R package "here" for more info
```

